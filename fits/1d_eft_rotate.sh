WKDIR=$1
RUNSCAN=$WKDIR/RooFitUtils/scripts/fit.py
RUNPLT=$WKDIR/RooFitUtils/scripts/plotscan.py
DATA=$2
DATAG=$3
TAG=$4
PLOTTAG=$5
OUT=$WKDIR/fits/20210130_fit/v5/$TAG/$DATAG/1d
PLT=$WKDIR/plots/20210130_fit/v5/$TAG/$DATAG/1d
#WS=$WKDIR/workspaces/20210126_smww_hww_eft_acc_linear_rot_v3.root
#WS=$WKDIR/workspaces/20210128_smww_hww_eft_acc_linear_rot_v4.root
#WS=$WKDIR/workspaces/20210128_smww_hww_eft_acc_linear_full_ev_v2.root
WS=$WKDIR/workspaces/20210130_smww_hww_eft_acc_linear_rot_v5_xs_reparam.root
POIS="cW_01 cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01 cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02 cll1_cHl3_01 cll1_cHl3_02 clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01 clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02"

CMN=" --workspace combined --input $WS --data $DATA --poi $POIS --no-findSigma --fit --reuse-nll --reuse-minimizer --offset --hesse --loglevel ERROR"
CMNPLT='--no-show-percent --no-show-sigma --atlas Internal --ymax 12'
DRW='--input '$PLOTTAG
mkdir -vp $PLT
mkdir -vp $PLT/texfiles

poinames=(c_{W} c_{HG\,uG\,HB\,HW\,HWB\,uH\,dH\,H\\Box\,HDD}^{[1]} c_{HG\,uG\,HB\,HW\,HWB\,uH\,dH\,H\\Box\,HDD}^{[2]} c_{{Hl}^{\(3\)}\,ll^{\(1\)}}^{[1]} c_{{Hl}^{\(3\)}\,ll^{\(1\)}}^{[2]} c_{lu\,lq^{\(3\)}\,lq^{\(1\)}\,ld\,Hd\,Hu\,q^{\(1\)}\,q^{\(3\)}}^{[1]} c_{lu\,lq^{\(3\)}\,lq^{\(1\)}\,ld\,Hd\,Hu\,q^{\(1\)}\,q^{\(3\)}}^{[2]});
pois=(cW_01 cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01 cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02 cll1_cHl3_01 cll1_cHl3_02 clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01 clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02)

#poilo=( -12.0 -0.02 -10.0 -1.5 -15.0 -1.0 -2.0 );
#poihi=( 12.0 0.02 10.0 1.5 15.0 1.0 2.0 );

poilo=(-6.96045 -0.01744085 -15.62522 -1.4713315 -15.84791 -0.9409136 -1.709102);
poihi=(22.44663 0.02539777 10.2364 1.5237185 18.96097 0.8785024 2.15945);
#poihi=(6.17 4.57 28.275 2.23 2.13 0.048 30.405);

npoints=70

mkdir -vp $OUT

## lines for submitting unconditional fit
#echo $RUNSCAN $CMN --output $OUT/unconditional_fit_$DATAG".txt" --writeResult
#echo $RUN $CMN --output $OUT/unconditional_fit.txt --writeResult

## loop over the POIs and submit scans for each POI
#for ((i=0; i<${#pois[@]}; i++)); do
#    $RUNSCAN $CMN --poi $POIS --scan ${pois[$i]} $npoints ${poilo[$i]} ${poihi[$i]} --jobSize 1 --writeSubmit $OUT/lines/${pois[$i]}/ --output $OUT/${pois[$i]}.txt && bsub sh $OUT/lines/${pois[$i]}/jobs.txt;
##  bsub -J $2"_"${pois[$i]} $RUNSCAN $CMN --poi ${pois[$i]} --scan ${pois[$i]} $npoints ${poilo[$i]} ${poihi[$i]}  --output $OUT/${pois[$i]}.txt
##   echo  ${pois[$i]} nll status >> $OUT/${pois[$i]}.txt && \
##   for x in $OUT/${pois[$i]}.txt.*; do
##   # echo $x; 
##    tail -1 $x >> $OUT/${pois[$i]}.txt;
## done
#done


## lines for plotting
for ((i=0; i<${#pois[@]}; i++)); do
 $RUNPLT $DRW $OUT/${pois[$i]}."*" --output $PLT/texfiles/${pois[$i]}.tex $CMNPLT --labels ${poinames[i]} && \
 pdflatex --interaction=batchmode -output-directory=$PLT/ $PLT/texfiles/${pois[$i]}.tex ;
done
rm $PLT/*.aux $PLT/*.log
