import ROOT
# dictionary of the parameterisation (here the number are the x-sec)
pardict={
"mu_ww_ptl0_00":{"SM":24.3247956496,"cuWAbs":0.00140209131807,"cHq3":5.3638464272,"cHq1":0.0869645614424,"cHd":-0.0275299139804,"cHG":7.24458824192,"cld":0.0016078671764,"cll1":5.84903222514,"cHW":-0.00841989133302,"clq3":0.0474540491745,"clq1":-0.0232928908757,"cuGAbs":0.286018824721,"cHDD":-0.0210419494838,"clu":-0.00341675695859,"cHl1":-0.00348417892937,"cHbox":0.0749827497088,"cHWB":0.0294947750443,"cuHAbs":-0.0221328268298,"cW":0.203676494328,"cHl3":-5.82999585544,"cHu":0.0580826368952},
"mu_ww_ptl0_01":{"SM":69.8987426758,"cuWAbs":0.00450356683719,"cHq3":15.9511435853,"cHq1":0.297122009747,"cHd":-0.0966928638817,"cHG":9.52905320808,"cld":0.00569044228732,"cll1":16.1894777411,"cHW":-0.00639998309936,"clq3":0.32764363812,"clq1":-0.0808856466206,"cuGAbs":0.455168726976,"cHDD":-0.0678585497533,"clu":-0.0133312215033,"cHl1":-0.00841965997716,"cHbox":0.13347209642,"cHWB":0.0532041627789,"cuHAbs":-0.0362775086253,"cW":0.465176968335,"cHl3":-16.3041816987,"cHu":0.207083316996},
"mu_ww_ptl0_02":{"SM":66.38256073,"cuWAbs":0.00600988266554,"cHq3":16.3825684976,"cHq1":0.326474178026,"cHd":-0.142368510863,"cHG":3.02929270586,"cld":0.0101111007235,"cll1":15.4790213288,"cHW":0.0113913657782,"clq3":0.545964232709,"clq1":-0.115718050943,"cuGAbs":0.0930696781312,"cHDD":-0.110412933056,"clu":-0.0236713369218,"cHl1":-0.00463300905437,"cHbox":0.0492679565296,"cHWB":0.00354193219003,"cuHAbs":-0.00990079444365,"cW":0.313457192602,"cHl3":-15.7113223125,"cHu":0.313791426383},
"mu_ww_ptl0_03":{"SM":50.3856124878,"cuWAbs":0.00673154404682,"cHq3":12.9856860646,"cHq1":0.188264016678,"cHd":-0.160165096704,"cHG":-0.193440356864,"cld":0.0129961537307,"cll1":12.0808417439,"cHW":0.0165450241581,"clq3":0.645175918279,"clq1":-0.124306243744,"cuGAbs":0.00612360517464,"cHDD":-0.12766943637,"clu":-0.0304041064433,"cHl1":-0.00203217920543,"cHbox":0.00744394169181,"cHWB":-0.0357201314166,"cuHAbs":0.00199551919306,"cW":0.0349651521319,"cHl3":-12.0081823278,"cHu":0.364851879862},
"mu_ww_ptl0_04":{"SM":35.4263801575,"cuWAbs":0.00592195994034,"cHq3":9.71012906032,"cHq1":0.0657335384228,"cHd":-0.153435171586,"cHG":-0.640848053454,"cld":0.0140414815189,"cll1":8.47791170389,"cHW":0.018753221081,"clq3":0.662136684824,"clq1":-0.138156911159,"cuGAbs":-0.0316279502298,"cHDD":-0.101759829903,"clu":-0.0339972314496,"cHl1":0.00146696859119,"cHbox":-0.00035519532973,"cHWB":-0.0388383113159,"cuHAbs":0.00317978477088,"cW":-0.123864886684,"cHl3":-8.48133211264,"cHu":0.358606048539},
"mu_ww_ptl0_05":{"SM":25.1162986755,"cuWAbs":0.0051555413102,"cHq3":7.24674040104,"cHq1":0.0667279847521,"cHd":-0.134428632383,"cHG":-0.560513204683,"cld":0.0146258946862,"cll1":5.82023708789,"cHW":0.0181758242867,"clq3":0.648052818408,"clq1":-0.14126434823,"cuGAbs":-0.019456952259,"cHDD":-0.0797093913124,"clu":-0.0359674576942,"cHl1":0.0017389886586,"cHbox":-0.000559962307223,"cHWB":-0.029852298948,"cuHAbs":0.00141342819883,"cW":-0.0897581624811,"cHl3":-5.74253103603,"cHu":0.318308536937},
"mu_ww_ptl0_06":{"SM":16.7873916626,"cuWAbs":0.0034080965807,"cHq3":5.22003202307,"cHq1":0.0147780560048,"cHd":-0.115027949241,"cHG":-0.650610808564,"cld":0.0141345810022,"cll1":3.90343340633,"cHW":0.0135618109548,"clq3":0.563202480739,"clq1":-0.128595598411,"cuGAbs":-0.0181123998845,"cHDD":-0.0594499550434,"clu":-0.0358366469324,"cHl1":0.000962826439599,"cHbox":0.00157838473156,"cHWB":-0.0214522664208,"cuHAbs":0.00111222672753,"cW":-0.145926373858,"cHl3":-3.83968583173,"cHu":0.2780711949},
"mu_ww_ptl0_07":{"SM":11.7213401794,"cuWAbs":0.00271450616056,"cHq3":3.8156705021,"cHq1":-0.00522102260436,"cHd":-0.0980334817777,"cHG":0.272578125377,"cld":0.0143413582903,"cll1":2.83806599717,"cHW":0.0115496369431,"clq3":0.528812346796,"clq1":-0.130061132712,"cuGAbs":-0.00262851808702,"cHDD":-0.048288305293,"clu":-0.0354127863423,"cHl1":0.001429848137,"cHbox":0.00163776766365,"cHWB":-0.017198783532,"cuHAbs":0.00146873904189,"cW":-0.130253068271,"cHl3":-2.86726365229,"cHu":0.238743779519},
"mu_ww_ptl0_08":{"SM":6.79176330566,"cuWAbs":0.00205313219072,"cHq3":2.45945774766,"cHq1":0.0261390755235,"cHd":-0.072896585053,"cHG":0.0765328358741,"cld":0.0125155443062,"cll1":1.66457645893,"cHW":0.00971765157995,"clq3":0.448412555876,"clq1":-0.104400079064,"cuGAbs":-0.00112859875525,"cHDD":-0.0300819048921,"clu":-0.0327405318394,"cHl1":0.0013741601722,"cHbox":0.000645487665851,"cHWB":-0.00641422135243,"cuHAbs":0.000297205536004,"cW":-0.087588819087,"cHl3":-1.67233674502,"cHu":0.180920575959},
"mu_ww_ptl0_09":{"SM":3.84351158142,"cuWAbs":0.00200227325985,"cHq3":1.47075263791,"cHq1":0.0115061808696,"cHd":-0.0510419910281,"cHG":0.0352576092799,"cld":0.0113567621548,"cll1":0.903266414522,"cHW":0.00657137264517,"clq3":0.379951509389,"clq1":-0.0967425979277,"cuGAbs":-0.00189385947547,"cHDD":-0.0155275561699,"clu":-0.030954713771,"cHl1":0.00113660481049,"cHbox":0.00214492547937,"cHWB":-0.00361585088109,"cuHAbs":0.000530585405016,"cW":-0.0818548251725,"cHl3":-0.8905387194,"cHu":0.129955929696},
"mu_ww_ptl0_10":{"SM":2.06479244232,"cuWAbs":0.00236023405256,"cHq3":0.936069084189,"cHq1":0.0324619081378,"cHd":-0.0359593678093,"cHG":0.0668497453376,"cld":0.0102495992529,"cll1":0.49290085953,"cHW":0.00432795898692,"clq3":0.314394303595,"clq1":-0.0799569690675,"cuGAbs":0.00262388073847,"cHDD":-0.00872190646303,"clu":-0.0280488362466,"cHl1":0.000976644207799,"cHbox":0.0014433036851,"cHWB":-0.00147233996335,"cuHAbs":7.02191069506e-05,"cW":-0.0408139565709,"cHl3":-0.505031420563,"cHu":0.0960582981734},
"mu_ww_ptl0_11":{"SM":0.923404481676,"cuWAbs":0.00192295407549,"cHq3":0.451423712118,"cHq1":0.0269671941013,"cHd":-0.0204937778726,"cHG":0.0534550109876,"cld":0.00782821638346,"cll1":0.218379659174,"cHW":0.00195505743144,"clq3":0.231445630937,"clq1":-0.0590370715469,"cuGAbs":0.00270519866945,"cHDD":-0.00417670364011,"clu":-0.0228465238992,"cHl1":0.000551972901864,"cHbox":0.00121289782425,"cHWB":-0.000228592765596,"cuHAbs":5.59216824959e-05,"cW":-0.0277992064231,"cHl3":-0.213334486064,"cHu":0.0573025672431},
"mu_ww_ptl0_12":{"SM":0.276277452707,"cuWAbs":0.000897237475272,"cHq3":0.196687324477,"cHq1":0.0245363616604,"cHd":-0.00912412362784,"cHG":0.0327552393085,"cld":0.00551278826498,"cll1":0.0652352353322,"cHW":0.000482120710979,"clq3":0.152526861896,"clq1":-0.0390914503487,"cuGAbs":0.00175068266476,"cHDD":-0.00129889577582,"clu":-0.0166950406027,"cHl1":0.000273646850576,"cHbox":0.000571583210763,"cHWB":-0.000338120561333,"cuHAbs":2.15030860663e-05,"cW":-0.0148199412662,"cHl3":-0.066548183549,"cHu":0.0280630242732},
"mu_ww_ptl0_13":{"SM":0.0120938037428,"cuWAbs":2.82564417146e-05,"cHq3":0.0142224305804,"cHq1":0.00361789066503,"cHd":-0.000880350676651,"cHG":0.00204110495981,"cld":0.00111058153432,"cll1":0.00380130503804,"cHW":4.22849156143e-06,"clq3":0.0305901712536,"clq1":-0.00707764981849,"cuGAbs":0.00010481523881,"cHDD":-8.80892762887e-05,"clu":-0.00379642047813,"cHl1":2.12028795455e-05,"cHbox":7.4113854703e-05,"cHWB":6.72478581685e-06,"cuHAbs":4.86229728576e-07,"cW":-0.000548334575635,"cHl3":-0.00328478988358,"cHu":0.00292389406145},
"mu_ww":{"SM":1.0,"cHDD":-0.0013,"cHd":-0.00416,"cll1":0.25,"cHl3":-0.2464,"cHq1":0.00636,"cHq3":0.26941,"cHu":0.009702,"cHWB":0.00051,"cW":0.00758},
"mu_gg2H":{"SM":1.0,"cW":0.0,"cHWB":0.0,"cHG":40.76,"cHW":0.0,"cHq1":0.0,"cHq3":0.0,"cHl1":0.0,"cHd":0.0,"cHu":0.0,"cll1":0.059,"cld":0.0,"clu":0.0,"clq1":0.0,"clq3":0.0,"cHl3":-0.061*2.0,"cHbox":0.061*2,"cHDD":-0.061*0.5,"cuHAbs":-0.061*2.01,"cuGAbs":1.606},
"mu_VBF":{"SM":1.0,"cHW":-0.087,"cHB":-0.002,"cHWB":0.024,"cHG":0.000,"cHDD":-0.011,"cHbox":0.121,"cHl1":0.000,"cHl3":-0.364,"cHe":0.000,"cHq1":0.008,"cHq3":-0.453,"cHu":-0.027,"cHd":0.010,"cll1":0.182,"ceHAbs":0.000,"cuHAbs":0.000,"cdHAbs":0.000,"cuGAbs":0.000,"cuWAbs":0.000,"cuBAbs":0.000,"cW":0.000},
"partial_width_ggF":{"SM":1.0,"cHDD":-0.03021,"cHW":-0.0992,"cHbox":0.12182,"cHl3":-0.22161,"cll1":0.18096},
"partial_width_VBF":{"SM":1.0,"cHDD":-0.0303,"cHW":-0.0902,"cHbox":0.121,"cHl3":-0.2292,"cll1":0.1824},
"total_width":{"SM":1.0,"cHW":-0.048,"cHB":-0.047,"cHWB": 0.044,"cHG":1.402,"cHDD":-0.030,"cHbox": 0.121,"cHl1":-0.000,"cHl3":-0.151,"cHe":-0.000,"cHq1": 0.000,"cHq3": 0.014,"cHu":0.000,"cHd":-0.000,"cll1": 0.075,"ceHAbs": -0.005,"cuHAbs": -0.012,"cdHAbs": -0.085,"cuGAbs":0.051,"cuWAbs": -0.002,"cuBAbs": -0.003,"cW":-0.001},
"norm_WW_0jet_HWW":{"SM":1.0,"cHDD":-0.0013,"cHd":-0.00416,"cll1":+0.25,"cHl3":-0.2464,"cHq1":+0.00636,"cHq3":+0.26941,"cHu":+0.009702,"cHWB":+0.00051,"cW":+0.00758}}

xsec_err={"norm_WW_0jet_HWW":0.0739545909046,"mu_VBF":3.99*10**-01,"mu_gg2H":2.48*10**-01,"mu_ww_ptl0_00":1.03*10**-01,"mu_ww_ptl0_01":8.81*10**-02,"mu_ww_ptl0_02":7.26*10**-02,"mu_ww_ptl0_03":8.05*10**-02,"mu_ww_ptl0_04":9.79*10**-02,"mu_ww_ptl0_05":9.23*10**-02,"mu_ww_ptl0_06":1.16*10**-01,"mu_ww_ptl0_07":1.18*10**-01,"mu_ww_ptl0_08":1.41*10**-01,"mu_ww_ptl0_09":1.39*10**-01,"mu_ww_ptl0_10":1.53*10**-01,"mu_ww_ptl0_11":1.85*10**-01,"mu_ww_ptl0_12":1.92*10**-01,"mu_ww_ptl0_13":3.26*10**-01}


allpars = set()
for ibin in pardict:
    for par in pardict[ibin]:
         allpars.add(par)

import copy

pardictpadded = copy.deepcopy(pardict)
for parbin in pardict.keys():
    for par in allpars:
        if par not in pardictpadded[parbin].keys():
            pardictpadded[parbin][par] = 0.0
            
import copy
pardictpadded_br = copy.deepcopy(pardictpadded)
#hwwpois = ["mu_gg2H","mu_gg2H_0J","mu_gg2H_1J_ptH_0_60","mu_gg2H_1J_ptH_gt60","mu_gg2H_ge2J","mu_VBF"]
hwwpois = ["norm_WW_0jet_HWW","mu_gg2H","mu_VBF"]

for poi in hwwpois:
    for par in pardictpadded["mu_gg2H"]:
        if par != "SM":
            if 'gg2H' in poi:
                pardictpadded_br[poi][par] = pardictpadded[poi][par] + pardictpadded["partial_width_ggF"][par] - pardictpadded["total_width"][par]
            if "VBF" in poi:
                pardictpadded_br[poi][par] = pardictpadded[poi][par] + pardictpadded["partial_width_VBF"][par] - pardictpadded["total_width"][par]
                
from atlasplots import atlas_style as astyle
from atlasplots import utils as utils
astyle.SetAtlasStyle()

ibins = []
for i in range(0,14):
    if i < 10:
        ibins.append("mu_ww_ptl0_0"+str(i))
    else:
        ibins.append("mu_ww_ptl0_"+str(i))

#hwwbins = ["gap1","mu_gg2H","gap2","mu_VBF","gap3","ggFHWW","VBFHWW"]
hwwbins = ["norm_WW_0jet_HWW","mu_gg2H","mu_VBF"]

# stxs-split - hwwbins = ["gap1","mu_gg2H_0J","mu_gg2H_1J_ptH_0_60","mu_gg2H_1J_ptH_gt60","mu_gg2H_ge2J","gap2","mu_VBF","gap3","partial_width_ggF","partial_width_VBF","total_width"]

for hwwbin in hwwbins:
    ibins.append(hwwbin)
#binlabels = ["30","40","50","60","70","80","90","100","120","140","160","180","200","300",">300","mu_{ww}"," ","gg#rightarrowH 0J","gg#rightarrowH 1J 0<p_{T}^{H}<60","gg#rightarrowH 1J p_{T}^{H}>60 && #geq 2J","gg#rightarrowH 1J p_{T}^{H}>60","gg#rightarrowH #geq 2J"," ","VBF"," ","BH#rightarrowWW#)"]
# bin labels for the x-axis
binlabels = ["30","40","50","60","70","80","90","100","120","140","160","180","200","300"," ","ggH","VBF"," ","#Gamma(H#rightarrowe#mu#nu#nu)_{ggF}","#Gamma(H#rightarrowe#mu#nu#nu)_{VBF}","#Gamma(H#rightarrowe#mu#nu#nu)_{VBF}"]

print len(ibins)

# grouping similar parameters in the same group
pargroups = {
    "group1":{'cld':(1.0,ROOT.kGreen),'clq1':(0.1,ROOT.kGreen+3),'clq3':(0.1,ROOT.kGreen+4),'clu':(1.0,ROOT.kGreen-9),'cHq3':(.1,ROOT.kRed-6),'cHu':(1.0,ROOT.kRed-5),'cHq1':(1.0,ROOT.kRed-8),'cHd':(1.0,ROOT.kRed),'cHl1':(10.0,ROOT.kPink-2)},
    "group2":{'cW':(5.0,ROOT.kMagenta),'cll1':(1.0,ROOT.kViolet),'cHl3':(1.0,ROOT.kViolet-9)},
    #"group1":{},
    "group3":{'cHW':(1.0,ROOT.kBlue-7),'cdHAbs':(1.0,ROOT.kBlue-9),'cHWB':(1.0,ROOT.kBlue-8),'cHB':(1.0,ROOT.kBlue-5),'cHDD':(1.0,ROOT.kCyan-2),'cHbox':(1.0,ROOT.kAzure+7),'cHG':(0.01,ROOT.kBlue),'cuGAbs':(0.1,ROOT.kAzure-4),'cuHAbs':(1.,ROOT.kBlue+4),'ceHAbs':(1.,ROOT.kCyan-6)},
     #,'ceHAbs':(1.,ROOT.kBlue-9),'cdHAbs':(1.,ROOT.kBlue-3),'cuWAbs':(1.,ROOT.kBlue+3),'cuBAbs':(1.,ROOT.kBlue+5),'cHe':(1.0,ROOT.kBlue+5)
}

labels={}
labels['cld']="c_{ld}"
labels['clq1']="c_{lq^{(1)}}"
labels['clq3']="c_{lq^{(3)}}"
labels['clu']="c_{lu}"
labels['cHq3']="c_{Hq^{(3)}}"
labels['cHu']="c_{Hu}"
labels['cHq1']="c_{Hq^{(1)}}"
labels['cHd']="c_{Hd}"
labels['cW']="c_{W}"
labels['cll1']="c_{ll^{(1)}}"
labels['cHl3']="c_{Hl^{(3)}}"
labels['cHl1']="c_{Hl^{(1)}}"


labels['cHW']="c_{HW}"
labels['cHWB']="c_{HWB}"
labels['cHDD']="c_{HDD}"
labels['cHB']="c_{HB}"
labels['cHbox']="c_{H\Box}"
labels['cHG']="c_{HG}"
labels['cuGAbs']="c_{uG}"
labels['cuHAbs']="c_{uH}"
labels['cdHAbs']="c_{dH}"
labels['ceHAbs']="c_{eH}"
labels['cHB']="c_{HB}"

orderpar = {
    "group1":['clq3','cHq3','cld','clq1','clu','cHu','cHq1','cHd'],
    "group2":['cHl3','cll1','cW'],
    "group3":['cHG','cuGAbs','cHW','cHWB','cHDD','cHB','cHbox','cuHAbs','cdHAbs']}

l = {}
box = {}
stackhistos = {}
histos = {}
legend = {}



# histogram for showing statistical uncertainities
errhisto,i = ROOT.TH1F("errhisto","errhisto",len(ibins),0,len(ibins)),0
for ibin in ibins:
    errhisto.Fill(i,xsec_err[ibin])
    errhisto.SetBinError(i,1e-06)
    i = i+1
    errhisto.SetBinError(i,1e-06)


for group in pargroups:
    stackhistos[group] = ROOT.THStack("hs"+"_"+group,"")
    histos[group], legend[group] = {}, {}
    ipar = 0

    # fill dummy histogram which acts as a spacer to the left
    for ihisto in range(0,(10-len(pargroups[group]))/2):
        histoname = "group_"+str(ihisto)
        histos[group][histoname], i = ROOT.TH1F(histoname,histoname,len(ibins),0,len(ibins)), 0
        for ibin in pardict:
            histos[group][histoname].Fill(0,0)
        histos[group][histoname].SetFillColor(ROOT.kWhite)
        stackhistos[group].Add(histos[group][histoname])


    # fill the histograms with the eft dependence
    for par in orderpar[group]:
        histos[group][par],i = ROOT.TH1F(par,par,len(ibins),0,len(ibins)), 0
        for ibin in ibins:
            histos[group][par].Fill(i,pargroups[group][par][0]*float(pardictpadded_br[ibins[i]][par])/float(pardictpadded_br[ibins[i]]["SM"]))
            i = i + 1
        # set legend for each parameter (looks ok)
        legend[group][par] = ROOT.TMathText(len(ibins)+0.2,0.5-0.125*ipar," "+labels[par] +"=" + str(pargroups[group][par][0]))
        legend[group][par].SetTextAlign(11)
        legend[group][par].SetTextColor(pargroups[group][par][1])
        legend[group][par].SetTextFont(43)
        legend[group][par].SetTextSize(8)
        legend[group][par].Draw()
        
        # color for each of the bar
        histos[group][par].SetFillColor(pargroups[group][par][1])
        stackhistos[group].Add(histos[group][par])
        ipar = ipar + 1
        
    # fill dummy histogram which acts as a spacer to the right
    for ihisto in range(0,(10-len(pargroups[group]))/2):
        histoname = "group_"+str(ihisto)
        histos[group][histoname], i = ROOT.TH1F(histoname,histoname,len(ibins),0,len(ibins)), 0
        for ibin in pardict:
            histos[group][histoname].Fill(0,0)
        histos[group][histoname].SetFillColor(ROOT.kWhite)
        stackhistos[group].Add(histos[group][histoname])


    
# filling in the histogram
cst0 = ROOT.TCanvas("cst0","cst0",700,500)

cst0.SetTickx()
cst0.SetLeftMargin(0.08)
cst0.SetRightMargin(0.1)
cst0.SetBottomMargin(0.0)
cst0.SetTopMargin(0.5)

tex1 = ROOT.TLatex()
tex1.SetTextSize(0.025)
tex1.SetTextFont(42)
tex1.SetTextColor(ROOT.kBlack)
tex1.DrawLatexNDC(0.22,0.96,"#sigma_{pp#rightarrowe^{#pm}#mu^{#mp}#nu#nu}")
#tex1.DrawLatexNDC(0.555,0.975,"#sigma_{ggH}")
#tex1.DrawLatexNDC(0.625,0.975,"#sigma_{VBF}")
tex1.DrawLatexNDC(0.68,0.96,"#sigma_{pp#rightarrowH#rightarrowe^{#pm}#mu^{#mp}#nu#nu}")


cst0.Divide(1,5,0,0)

t3 = ROOT.TLatex()
t3.SetTextFont(72)
t3.SetTextColor(ROOT.kBlack)
t3.SetTextSize(0.028)
t3.DrawLatexNDC(0.068,0.90,"ATLAS")
t3.SetTextFont(42)
t3.DrawLatexNDC(0.14,0.90,"Internal")
t3.SetTextSize(0.025)
t3.DrawLatexNDC(0.07,0.87,"#sqrt{s}=13 TeV 36.1 fb^{-1}")

# first pad for the error bars
pad = cst0.cd(1)

pad.SetTopMargin(0.01)
pad.SetBottomMargin(0.01)
pad.SetLeftMargin(0.06)
pad.SetRightMargin(0.16)
pad.SetGridx()

errhisto.SetFillStyle(3004)
errhisto.SetFillColor(ROOT.kGray+1) 
errhisto.SetYTitle("#sigma_{}")

errhisto.SetMinimum(1e-4)
errhisto.SetMaximum(0.5*1.14)

errhisto.SetMarkerSize(0)
errhisto.GetXaxis().SetLabelSize(0)
errhisto.GetXaxis().SetNdivisions(len(ibins),0) 
errhisto.GetYaxis().SetLabelSize(0.150)
errhisto.GetYaxis().SetTitleSize(0.150)
errhisto.GetYaxis().SetTitleOffset(0.20)
errhisto.GetYaxis().SetTickSize(0.005) 
errhisto.SetLineColor(ROOT.kWhite)
errhisto.Draw("hist same")
errhisto.SetLineColor(ROOT.kGray+1)
errhisto.SetMarkerColor(ROOT.kGray+1)
errhisto.Draw("e same")

for i in range(0,5):
    box[str(i)] = {}
#box["0"]["1"] = ROOT.TBox(14.02,0.0,15,1.14*0.49)
#box["0"]["2"] = ROOT.TBox(16.02,0.02,17,1.14-0.02)
#box["0"]["3"] = ROOT.TBox(18.02,0.02,19,1.14-0.02)

#box["0"]["1"].Draw("same")
#box["0"]["2"].Draw("same")
#box["0"]["3"].Draw("same")

pad.Draw()
# fill in the remaining pads with the eft impact plots
for i in range(2,5):
    
    pad = cst0.cd(i)
    pad.SetGridx()
    pad.SetTopMargin(0.01)
    pad.SetBottomMargin(0.02)
    pad.SetLeftMargin(0.06)
    pad.SetRightMargin(0.16)

    grpname = "group"+str(i-1)

    stackhistos[grpname].Draw("nostackbhist")
    for par in legend[grpname]:
        legend[grpname][par].Draw()
   # ic = 0
   # for x in ibins:
   #     ic = ic + 1
   #     stackhistos[grpname].GetXaxis().SetBinLabel(ic,binlabels[ic])
    
    stackhistos[grpname].GetXaxis().SetLabelSize(0)
    stackhistos[grpname].GetXaxis().SetNdivisions(len(ibins),0)

    stackhistos[grpname].SetMinimum(-0.5*1.14)
    stackhistos[grpname].SetMaximum(0.5*1.14)
    stackhistos[grpname].GetYaxis().SetLabelSize(0.12)
    stackhistos[grpname].GetYaxis().SetTitleSize(0.12)
    stackhistos[grpname].GetYaxis().SetTitle("#Delta(#sigma#timesB)(c_{i})/SM")
    stackhistos[grpname].GetYaxis().SetTitleOffset(0.23)
    stackhistos[grpname].GetYaxis().SetTickSize(0.01)

    #stackhistos[grpname].GetYaxis().SetTitle("#delta #sigma_{EFT} /#sigma_{SM} ")

#    stackhistos[grpname].SetMinimum(-1)
#    stackhistos[grpname].SetMaximum(+1)
#    stackhistos[grpname].GetXaxis().SetLabelOffset(0.04)
#    stackhistos[grpname].Draw("axis same")
#    stackhistos[grpname].Draw("frame same")
    l[grpname] = ROOT.TLine(0.0,0.0,float(len(ibins)),0)
    l[grpname].SetLineColor(ROOT.kBlack)
    l[grpname].Draw("same")

    range1 = 1.14*0.5
#    box[str(i)]["1"] = ROOT.TBox(14.02,range1*-0.985,15,range1*0.98)
    #box[str(i)]["2"] = ROOT.TBox(16.02,range1*-0.985,17,range1*0.98)
    #box[str(i)]["3"] = ROOT.TBox(18.02,range1*-0.985,19,range1*0.98)

    #box[str(i)]["1"].Draw("same")
    #box[str(i)]["2"].Draw("same")
    #box[str(i)]["3"].Draw("same")

cst0.cd()
tex2 = ROOT.TLatex()
for i in range(0,len(ibins)):
 #   tex2.SetTextAngle(-45)
    tex2.SetTextFont(42)
    tex2.SetTextSize(0.015)
    tex2.SetTextColor(ROOT.kBlack)
    if i > 18:
        tex2.SetTextAngle(-45)
        tex2.DrawLatexNDC(0.0435+0.04*i,0.16,binlabels[i])
    if i > 13:
        tex2.DrawLatexNDC(0.065+0.0415*i,0.17,binlabels[i])
    else:
        tex2.DrawLatexNDC(0.0525+0.0415*i,0.17,binlabels[i])


tex2.SetTextAngle(0)
tex2.DrawLatexNDC(0.58,0.14,"p_{T}^{lead l} [GeV]")
tex2.DrawLatexNDC(0.64,0.17,"norm.")
tex2.DrawLatexNDC(0.635,0.155,"WW 0-jet")


cst0.Draw()
cst0.SaveAs("impact_ww_SMWW_workspace_vNLO.eps")
