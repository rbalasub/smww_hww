from stxspar import stxspoi

from RooFitUtils.io import getCorrelation,getCovariance

obs = "kunlin/corrmat/WS-Comb-STXSChannel_mu_merge_pruned_data_unconditionalfit.root"
asm = "kunlin/corrmat/WS-Comb-STXSChannel_mu_merge_pruned_asimov_unconditionalfit.root"

summary_obs = "kunlin/summary/STXSxBRMuData.txt"
summary_asm = "kunlin/summary/STXSxBRMuAsimov.txt"

from RooFitUtils.io import readsummary

results = {"obs":readsummary(summary_obs),"asm":readsummary(summary_asm)}
colors = ["myotherblue","myotheryellow","cyan","red"]

from RooFitUtils.io import texdef
texdef("hzz","$H\\to ZZ$")
texdef("hyy","$H\\to \\gamma\\gamma$")
texdef("hbb","$H\\to b\\bar{b}$")

allpars = []
for group in stxspoi:
    for poi in group[-1]:
        allpars.append(poi)
        texdef(poi,"$\\mu_{"+poi[3:].replace("_","")+"}$")
allpars.sort()

labels=["$\\sqrt{s}=$13 TeV, 139 fb$^{{-1}}$",
        "$m_{\\scalebox{.9}{$H$}}=$ 125.09 GeV, $|y_{\\scalebox{.9}{$H$}}|$ $<$ 2.5",
        "SMEFT $\\Lambda=1$ TeV"    ]

for cat,res in results.items():
    data = []
    data.append(({"label":cat+"_stat","point":False,"error":True,"style":"line width=4pt","color":"myotheryellow","labelshift":"9em"},results[cat]["stat"]))
    data.append(({"label":cat+"_sys" ,"point":False,"error":True,"style":"line width=2pt","color":"myotherblue","labelshift":"6em"},results[cat]["sys"]))
    data.append(({"label":cat,"point":True,"error":True,"style":"|-|","color":"black","labelshift":"3em"},results[cat]["cv"]))
    from RooFitUtils.pgfplotter import writepois
    writepois("Internal",stxspoi,data,"stxssummary_"+cat+".tex",labels,smval=1,range=(-2,10),spread=0.5,printvalues=True)

    from RooFitUtils.hepdatawriter import writepois as writehepdatapois
    from RooFitUtils.csvwriter import writepois as writecsvpois    
    writehepdatapois(stxspoi,data,"stxssummary_"+cat+".yml")
    writecsvpois(stxspoi,data,"stxssummary_"+cat+".data")    
    

from RooFitUtils.pgfplotter import writecorrmatrix
from RooFitUtils.hepdatawriter import writematrix as writehepdatamatrix
from RooFitUtils.csvwriter import writematrix as writecsvmatrix

corr_asm = getCorrelation(obs,parameters=allpars)
cov_asm = getCovariance(obs,parameters=allpars)
writecorrmatrix("Internal",stxspoi,corr_asm,"correlations-stxs-asimov.tex")
writehepdatamatrix(stxspoi,stxspoi,corr_asm,"correlations-stxs-asimov.yml",label="correlations")
writecsvmatrix(allpars,allpars,corr_asm,"correlations-stxs-asimov.data")
writecsvmatrix(allpars,allpars,cov_asm,"covariance-stxs-asimov.data")

corr_obs = getCorrelation(obs,parameters=allpars)
cov_obs = getCorrelation(obs,parameters=allpars)
writecorrmatrix("Internal",stxspoi,corr_obs,"correlations-stxs-observed.tex")
writehepdatamatrix(allpars,allpars,corr_obs,"correlations-stxs-observed.yml",label="correlations")
writecsvmatrix(allpars,allpars,corr_obs,"correlations-stxs-observed.data")
writecsvmatrix(allpars,allpars,cov_obs,"covariance-stxs-observed.data")
