def reducedict(d):
    retval = {}
    for k,v in d.items():
        try:
            retval[k] = float(v)
        except ValueError:
            pass
    return retval

def readmatrix(infilename):
    import csv
    param = {}
    with open(infilename) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            param[row["parameter"]] = reducedict(row)
    return param

param = readmatrix("atlas-conf-2020-053-data/parametrization.data")
stxscov = readmatrix("atlas-conf-2020-053-data/covariance-stxs-asimov.data")

bins = sorted(list(stxscov.keys()))
parnames = sorted(list(param.keys()))

parametrization_full = []
parametrization_lin  = []
for p in parnames:
    p_full = []
    p_lin  = []    
    for b in bins:
        suffix = b[-2:]
        binname = b[3:-3]
        prod = param[p][binname]
        dec = param[p][suffix]
        p_full.append((1+prod)*(1+dec))
        p_lin.append(1+prod+dec)
    parametrization_full.append(p_full)
    parametrization_lin.append(p_lin)

from numpy import array
stxsmat = array([ [stxscov[i][j] for i in bins] for j in bins ])
parametrization = array(parametrization_lin)

from RooFitUtils.pgfplotter import writematrix

writematrix("Internal",bins,parnames,
            parametrization,
            "param.tex",centralval=1)


from numpy import linalg as LA
from numpy import real

covmat = (parametrization@stxsmat)@parametrization.transpose()

evals,evecs = LA.eig(covmat)

writematrix("Internal",parnames,parnames,
            real(evecs),
            "decomp.tex",-1,1,45)
#writecsvmatrix(parnames,parnames,decomp,"decomposition.data")




    
