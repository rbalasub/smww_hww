def readlinearcomb(lcomb):
    import re
    term = re.compile("[ ]*([+-]?[ ]*[0-9.]+)[ ]*(\\\\[A-z0-9{}]+)")
    terms = term.findall(lcomb)
    return {t[1].replace("\\",""):float(t[0].replace(" ","")) for t in terms}
    

def main(args):
    from RooFitUtils.io import readtables    

    tables = readtables(args.inputs)

    params = {}
    
    for table in tables:
        params[" ".join(table[0]).replace("\\text","").replace("\\","")] = { row[0].replace("\\geq",">=").replace("\\",""):readlinearcomb(row[1]) for row in table[1:] if len(row)>1}

    from RooFitUtils.csvwriter import writecsv
    from RooFitUtils.util import flattened
    
    for tablename,table in params.items():
        outfilename = tablename.replace("/","").replace("  "," ").replace(" ","_")+".data"
        keys = sorted(list(set(flattened([row.keys() for row in table.values()]))))
        tab = [ [ tablename ] + list(keys) ] + [ [ k ] + [ str(table[k].get(c,0)) for c in keys ] for k in table.keys() ]
        writecsv(outfilename,tab)
            
    
from argparse import ArgumentParser
parser = ArgumentParser()
parser.add_argument("inputs",nargs="+")
main(parser.parse_args())
