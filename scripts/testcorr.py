import ROOT

_memory = []
def protect(obj):
    _memory.append(obj)

def makews(corr,nevents):
    ws = ROOT.RooWorkspace("playground")
    x = ws.factory("x[-1,1]")
    y = ws.factory("y[-1,1]")
    ws.defineSet("obs","x,y")
    
    mu_x = ws.factory("mu_x[-1,1]")
    mu_y = ws.factory("mu_y[-1,1]")
    ws.defineSet("poi","mu_x,mu_y")
   
    corrmat = ROOT.TMatrixDSym(2)
    for i in range(0,len(ws.set("obs"))):
        corrmat[i][i] = 1

    corrmat[0][1] = corr
    corrmat[1][0] = corr

    corrmat.Print()
    
    multigauss = ROOT.RooMultiVarGaussian("model","Test",ws.set("obs"),ws.set("poi"),corrmat)
    getattr(ws,"import")(multigauss,ROOT.RooFit.RecycleConflictNodes())

    asimov = multigauss.generate(ws.set("obs"),nevents)
    asimov.SetName("asimov")
    getattr(ws,"import")(asimov)    
    
    return ws

def contour(ws,mini):
    plot = mini.contour(ws.var("mu_x"),ws.var("mu_y"))
    plot.Draw()

def corrmat(fr):
    fr.correlationHist().Draw("COLZ")
    ROOT.gStyle.SetOptStat(False)
    ROOT.gStyle.SetPalette(ROOT.kTemperatureMap)

def scan(mini,nll,nllmin,poi,otherpoi):
    n = 11
    poival = poi.getVal()
    otherpoival = otherpoi.getVal()
    error = poi.getError()
    scale = otherpoi.getError()
    nllscan = ROOT.TGraph(n)
    nllscan.SetName("nllscan")
    nllscan.SetTitle("nllscan;"+poi.GetName()+";#Lambda")
    protect(nllscan)
    poiscan = ROOT.TGraph(n)
    poiscan.SetName("poiscan")
    step = 1./(n-1)
    poiscan.SetTitle("poiscan;"+poi.GetName()+"/#sigma_{"+poi.GetName()+"};"+otherpoi.GetName()+"/#sigma_{"+otherpoi.GetName()+"}")
    protect(poiscan)
    for i in range(0,n):
        xval = poi.getMin() + step*i*(poi.getMax()-poi.getMin())
        poi.setVal(xval)
        poi.setConstant(True)
        mini.minimize("Minuit2","Migrad")
        nllval = nll.getVal()-nllmin
        nllscan.SetPoint(i,xval,nllval)
        poiscan.SetPoint(i,(xval-poival)/error,(otherpoi.getVal()-otherpoival)/scale)        
    return nllscan,poiscan

def scan2d(nll,nllmin,poi,otherpoi):
    n = 11
    step = 1./(n-1)    
    nllscan = ROOT.TH2F("nllscan2d","nllscan2d",n,poi.getMin(),poi.getMax(),n,otherpoi.getMin(),otherpoi.getMax())
    protect(nllscan)
    for i in range(0,n+1):
        xval = poi.getMin() + step*i*(poi.getMax()-poi.getMin())
        poi.setVal(xval)
        for j in range(0,n+1):
            yval = otherpoi.getMin() + step*j*(otherpoi.getMax()-otherpoi.getMin())        
            otherpoi.setVal(yval)
            nllscan.SetBinContent(i,j,nll.getVal()-nllmin)
    return nllscan

def pulls():
    pass
    
ws = makews(.3,100)
for poi in ws.set("poi"):
    poi.setVal(.1)
    poi.setError(.1)
pdf = ws.pdf("model")
data = ws.data("asimov")

nll = pdf.createNLL(data)

mini = ROOT.RooMinimizer(nll)

mini.minimize("Minuit2","Migrad")
fr = mini.save()
nllmin = nll.getVal()
mini.minos()



c = ROOT.TCanvas("test")
c.Divide(2,2)

c.cd(1)
contour(ws,mini)

c.cd(3)
corrmat(fr)

nllscan,poiscan = scan(mini,nll,nllmin,ws.var("mu_x"),ws.var("mu_y"))

c.cd(2)
nllscan.Draw("AL")

c.cd(4)
poiscan.Draw("AL")

c.SaveAs("plot.pdf")
c.SaveAs("plot.png")


c = ROOT.TCanvas("test2d")
c.cd()
hist = scan2d(nll,nllmin,ws.var("mu_x"),ws.var("mu_y"))
hist.Draw("COLZ")
c.SaveAs("test2.pdf")
c.SaveAs("test2.png")
