from evpar import evs,fullevs,eigenvalues
from RooFitUtils.util import formatNumberPDG

def formatInterval(x):
    return "["+formatNumberPDG(x[0])+","+formatNumberPDG(x[1])+"]"

from RooFitUtils.pgfplotter import writematrix
from RooFitUtils.hepdatawriter import writematrix as writehepdatamatrix
from RooFitUtils.csvwriter import writematrix as writecsvmatrix
import ROOT
from os.path import join as pjoin

sorting = ['cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01', 'cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02','clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01', 'clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02','cll1_cHl3_01', 'cll1_cHl3_02','cW_01']

ev_names = ['cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01', 'cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02','clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01', 'clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02','cll1_cHl3_01', 'cll1_cHl3_02','cW_01']

parnames = [
    'cW',    
    'cHl3',
    'cll1',
    'cHu',
    'cHq1',
    'cHq3',
    'cHd',
    'clu',
    'clq3',
    'clq1',
    'cld',
    'cuHAbs',
    'cdHAbs',
    'cHbox',
    'cHWB',
    'cHW',
    'cHDD',
    'cHB',
    'cuGAbs',
    'cHG']

moreparnames = [ "cHbox", "cdHAbs" ]

allpars = sorted(list(set([elem[1] for ev in fullevs.values()  for elem in ev])))
allpars_sorted = [ x for x in parnames ]
for x in moreparnames:
    if x in allpars and not x in allpars_sorted:
        allpars_sorted.append(x)
for x in allpars:
    if not x in allpars_sorted:
        allpars_sorted.append(x)        

ev_names.reverse()
evlabels = [ "{$\\lambda=" + str(eigenvalues[i]) + "$"+" \#" + str(i+1)+"}" for i in range(0,len(eigenvalues)) ]
evkeys = [ "ev"+str(i+1) for i in range(0,len(eigenvalues)) ]
evkeys.reverse()
evlabels.reverse()

def convert(ev_names,parnames,evs):
    allvals = [ ]
    for ev in ev_names:
        d = { e[1]:e[0] for e in evs[ev] }
        vals = []
        for p in parnames:
            if p in d.keys():
                vals.append(d[p])
            else:
                vals.append(0.)
        allvals.append(vals)
    return allvals


parametrisation = convert(ev_names,parnames,evs)

from numpy import array
from numpy.linalg import inv,pinv
#from RooFitUtils.util import correlation_from_covariance 
mat = array(parametrisation)
matinv = pinv(mat)

matlabels=["$\\sqrt{s}=$13 TeV, 36 fb$^{\\scriptsize{-1}}$"]

writematrix("Internal",parnames,ev_names,
            mat,
            "rotation.tex",-1,1,45,plotlabels=matlabels)
#writehepdatamatrix(parnames,ev_names,mat,"rotation.yml",label="rotation")
#writecsvmatrix(parnames,ev_names,mat,"rotation.data")

#writematrix("Internal",ev_names,parnames,
#            matinv,
#            "invrotation.tex",-1,1,45,plotlabels=matlabels)

file0 = ROOT.TFile.Open("/project/atlas/users/rahulb/project/smww_hww/results/smww_hww_exp_fitresult.root")
fitres = file0.Get("fitresult_minimizer_AsimovData")
poiList = ROOT.RooArgList()
pois = ['mu_VBF','mu_gg2H','mu_ww_ptl0_00','mu_ww_ptl0_01','mu_ww_ptl0_02','mu_ww_ptl0_03','mu_ww_ptl0_04','mu_ww_ptl0_05','mu_ww_ptl0_06','mu_ww_ptl0_07','mu_ww_ptl0_08','mu_ww_ptl0_09','mu_ww_ptl0_10','mu_ww_ptl0_11','mu_ww_ptl0_12','mu_ww_ptl0_13']
for poi in (pois):
    poifound = fitres.floatParsFinal().find(poi)
    if poifound:
        poiList.add(poifound)
    else:
        print("Not found "+poi)

mat = fitres.reducedCovarianceMatrix(poiList)
covmat = []
for icol in range(0,mat.GetNcols()):
    arr = []
    for irow in range(0,mat.GetNrows()):
        arr.append(mat[irow][icol])
    covmat.append(arr)
allevs = list(fullevs.keys())
#corrmat = correlation_from_covariance(covmat)

#writematrix("Internal",pois,pois[::-1],corrmat[::-1],"corrmat.tex",-1,1,45,plotlabels=matlabels)

with open("decomposition-table.tex","wt") as decomptable:
    decomptable.write("\\documentclass{standalone}\n")
    decomptable.write("\\begin{document}\n")    
    decomptable.write("\\begin{tabular}{"+"l"*len(eigenvalues)+"}\n")    
    for i in range(0,len(allevs)):
        decomptable.write(str(i+1)+"&"+str(eigenvalues[i])+"&")
        for val,c in fullevs[allevs[i]]:
            if val>0: decomptable.write("+")
            decomptable.write(str(val)+str(c))
        decomptable.write("\\\\\n")
    decomptable.write("\\end{tabular}\n")    
    decomptable.write("\\end{document}\n")    
allevs.reverse()

decomp = convert(allevs,allpars_sorted,fullevs)
writematrix("Internal",allpars_sorted,evkeys,
            decomp,
            "decomposition.tex",-1,1,45,plotlabels=matlabels)
writehepdatamatrix(allpars_sorted,evkeys,decomp,"decomposition.yml","eigenvector_entries")
writecsvmatrix(allpars_sorted,evkeys,decomp,"decomposition.data")

from RooFitUtils.io import collectresults,collectcorrelations
from RooFitUtils.util import getThreshold,getPercent
t1sigma = getThreshold(getPercent(1),1)
t2sigma = getThreshold(getPercent(2),1)
t68perc = getThreshold(.68,1)
t95perc = getThreshold(.95,1)

labels=["$\\sqrt{s}=$13 TeV, 36 fb$^{\\scriptsize{-1}}$",
        "$m_{\\scalebox{.9}{$\\scriptstyle H$}}=$ 125.09 GeV, $|y_{\\scalebox{.9}{$\\scriptstyle H$}}|$ $<$ 2.5",
        "SMEFT $\\Lambda=1$ TeV"    ]

def getResults(values):
    from RooFitUtils.util import graphrescale,make1dscan
    from RooFitUtils.interpolate import findcrossings,findminimum,findintervals,findallminima
    allpoints = make1dscan(values)
    nllmin = findminimum(allpoints)
    points = graphrescale(allpoints,nllmin,2)
    cv,dn,up = findcrossings(points,t1sigma)
    allcv = findallminima(points,0,0.05)
    i1s = findintervals(points,t68perc)
    i2s = findintervals(points,t95perc)
#    i1s = findintervals(points,t1sigma)
#    i2s = findintervals(points,t2sigma)
    return allcv,i1s,i2s

colors = ["myblue","myyellow","cyan","red"]

def makeSummary(base,plottype,directories,ev_pois,ev_pois_list):
    scans = {}
    fitresults = {}
    paths = []
    results = {d:{"cv":{},"1s":{},"2s":{}} for d in directories }
    for name in ev_pois.keys():
        for d in directories:
            path = pjoin(base,d,plottype,name+".txt*")
            collectresults(scans,fitresults,path,"scan_"+d+"_"+name)
            try:
                results[d]["cv"][name],results[d]["1s"][name],results[d]["2s"][name] = getResults(scans[(name,)]["scan_"+d+"_"+name])
            except Exception as e:
                print("failed to prepare results for "+d+", reason: "+str(e))

    pois10 = [ (k,ev_pois[k].copy()) for k in ev_pois_list if ev_pois[k].get("scale",1) > 1 ]
    pois1 =  [ (k,ev_pois[k].copy()) for k in ev_pois_list if ev_pois[k].get("scale",1) == 1 ]
    pois01 = [ (k,ev_pois[k].copy()) for k in ev_pois_list if ev_pois[k].get("scale",1) < 1 ]
    for k,p in pois10:
        p["scale"] = p["scale"]/10
    for k,p in pois01:
        p["scale"] = p["scale"]*10

    data = []
    for i in range(0,len(directories)):
        d = directories[i]
        c = colors[i]
        offset = 3. * (1 - 2.*i/(len(directories)))#-1))
        data.append(({"label":d+"_95pCI","point":False,"error":False,"interval":True,"style":"thick,densely dotted,yshift="+str(offset)+"pt","color":c},results[d]["2s"]))
        data.append(({"label":d+"_68pCI","point":False,"error":False,"interval":True,"style":"ultra thick,yshift="+str(offset)+"pt","color":c},results[d]["1s"]))
        data.append(({"label":d,"point":True,"error":False,"style":"yshift="+str(offset)+"pt","color":c},results[d]["cv"]))

    from RooFitUtils.pgfplotter import writepois
    writepois("Internal",pois10,data,"eftSummary_"+plottype+"_EV_10.tex",labels,range=[-.15,.15])
    writepois(None,pois1,data,"eftSummary_"+plottype+"_EV_1.tex",labels,range=[-1.5,1.5])
    writepois(None,pois01,data,"eftSummary_"+plottype+"_EV_01.tex",labels,range=[-15,15])

    #from RooFitUtils.hepdatawriter import writepois as writehepdatapois
    #from RooFitUtils.csvwriter import writepois as writecsvpois    
    #writehepdatapois(ev_pois,data,"eftSummary_"+plottype+".yml")
    #writecsvpois(ev_pois,data,"eftSummary_"+plottype+".data")    
    
    return results
    
def makeErrorTable(filename,tot,stat):
    from math import sqrt
    with open(filename,"wt") as table:
        table.write("\\documentclass{article}\n")    
        table.write("\\usepackage{adjustbox}\n")    
        table.write("\\usepackage{array}\n")    
        table.write("\\usepackage{siunitx}\n")    
        table.write("\\begin{document}\n")
        table.write("\\renewcommand\\arraystretch{1.5}\n")
        table.write("\\begin{adjustbox}{width=\\textwidth}\n")
        table.write("\\begin{tabular}{l S[table-format=3.2] l l l }\n")
        table.write("coefficient & obs cv & stat & sys & tot\\\\\n")
        for name in sorting:
            try:
                cvtot  = tot["cv"][name][0]
                cvstat = stat["cv"][name][0]                
                table.write(name.replace("_"," "))
                table.write(
                    " &" + formatNumberPDG(cvtot)
                    + "&${}^{+"
                    + formatNumberPDG( stat["1s"][name][-1][1]-cvstat ) +"}_{"
                    + formatNumberPDG( stat["1s"][name][ 0][0]-cvstat ) +"}"
                    + "$&${}^{+"
                    + formatNumberPDG( sqrt(abs(pow(tot["1s"][name][-1][1]-cvtot,2) - pow(stat["1s"][name][-1][1]-cvstat,2))))+"}_{"
                    + formatNumberPDG(-sqrt(abs(pow(tot["1s"][name][ 0][0]-cvtot,2) - pow(stat["1s"][name][ 0][0]-cvstat,2))))+"}"
                    + "$&${}^{+"
                    + formatNumberPDG( tot["1s"][name][-1][1]-cvtot ) +"}_{"
                    + formatNumberPDG( tot["1s"][name][ 0][0]-cvtot ) +"}"                    
                    + "$")
                table.write("\\\\\n")
            except KeyError:
                continue
        table.write("\\end{tabular}\n")
        table.write("\\end{adjustbox}\n")    
        table.write("\\end{document}\n")
        print("wrote "+filename)

def ltx(s):
    return s.replace("_"," ")

def makeCSV(filename,obs,exp,keys):
    with open(filename,"wt") as table:
        table.write("coefficient ")
        for k in keys:
            table.write(", obs "+ltx(k)+" cv , obs "+ltx(k)+" 68 , obs "+ltx(k)+" 95 , exp "+ltx(k)+" 68 , exp "+ltx(k)+" 95 ")
        table.write("\n")
        for name in sorting:
            table.write(name.replace("_"," "))
            for k in keys:
                table.write(
                    " ," + formatNumberPDG(obs[k]["cv"][name][0])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in obs[k]["1s"][name]]) 
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in obs[k]["2s"][name]]) 
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in exp[k]["1s"][name]])
                    + "," + "v".join(["["+str(x[0])+","+str(x[1])+"]" for x in exp[k]["2s"][name]])
                )
            table.write("\n")

def makeTable(filename,obs,exp,keys):
    with open(filename,"wt") as table:
        table.write("\\documentclass{article}\n")    
        table.write("\\usepackage{adjustbox}\n")    
        table.write("\\begin{document}\n")
        table.write("\\begin{adjustbox}{width=\\textwidth}\n")
        table.write("\\begin{tabular}{l " + "l"*5*len(keys) + "}\n")
        table.write("coefficient ")
        for k in keys:
            table.write("& obs "+ltx(k)+" cv & obs "+ltx(k)+" 68 & obs "+ltx(k)+" 95 & exp "+ltx(k)+" 68 & exp "+ltx(k)+" 95 ")
        table.write("\\\\\n")
        for name in sorting:
            table.write(name.replace("_"," "))
            for k in keys:
                table.write(
                    " &$" + str(obs[k]["cv"][name][0])
                    + "$&$" + "\wedge".join([formatInterval(x) for x in obs[k]["1s"][name]]) 
                    + "$&$" + "\wedge".join([formatInterval(x) for x in obs[k]["2s"][name]]) 
                    + "$&$" + "\wedge".join([formatInterval(x) for x in exp[k]["1s"][name]])
                    + "$&$" + "\wedge".join([formatInterval(x) for x in exp[k]["2s"][name]])
                    + "$")
            table.write("\\\\\n")
        table.write("\\end{tabular}\n")
        table.write("\\end{adjustbox}\n")
        table.write("\n")            
        table.write("\\begin{adjustbox}{width=\\textwidth}\n")    
        table.write("\\begin{tabular}{l " + "l"*3*len(keys) + "}\n")
        table.write("coefficient ")
        for k in keys:
            table.write("& obs "+ltx(k)+" cv & obs "+ltx(k)+" 68 & obs "+ltx(k)+" 95 ")
        table.write("\\\\\n")        
        for name in sorting:
            table.write(name.replace("_"," "))
            for k in keys:
                table.write(
                    " &$" + formatNumberPDG(obs[k]["cv"][name][0])
                    + "$&$" + "\wedge".join([formatInterval(x) for x in obs[k]["1s"][name]]) 
                    + "$&$" + "\wedge".join([formatInterval(x) for x in obs[k]["2s"][name]]) 
                    + "$")
            table.write("\\\\\n")
        table.write("\\end{tabular}\n")
        table.write("\\end{adjustbox}\n")    
        table.write("\n")            
        table.write("\\begin{adjustbox}{width=\\textwidth}\n")
        table.write("\\begin{tabular}{l " + "l"*2*len(keys) + "}\n")
        table.write("coefficient ")
        for k in keys:
            table.write(" & exp "+ltx(k)+" 68 & exp "+ltx(k)+" 95 ")
        table.write("\\\\\n")        
        for name in sorting:
            table.write(name.replace("_"," "))
            for k in keys:
                table.write(
                    " &$" + "\wedge".join([formatInterval(x) for x in exp[k]["1s"][name]])
                    + "$&$" + "\wedge".join([formatInterval(x) for x in exp[k]["2s"][name]])
                    + "$")
            table.write("\\\\\n")
        table.write("\\end{tabular}\n")
        table.write("\\end{adjustbox}\n")    
        table.write("\\end{document}\n")        
    print("wrote "+filename)    

def makeBackRotation(ev_results_cv):
    ev_vals = [ ev_results_cv[name] for name in ev_names ]
    c_pois = { c:{} for c in parnames }
    
    from RooFitUtils.io import getCovariance
    from numpy import matmul

    covmat = getCovariance("20201001_WS-Comb_rotated1_BSM_obs_qf_set1.root","fitResult",ev_names)
    hesse = inv(covmat)
    c_cov = matmul(matmul(matinv,hesse),matinv.transpose())
    c_errs = [ c_cov[i][i] for i in range(0,len(parnames)) ]
    
    c_vals = matmul(matinv,ev_vals)
    c_results = { parnames[i] : (c_vals[i],-c_errs[i],c_errs[i]) for i in range(0,len(parnames)) }

    from RooFitUtils.pgfplotter import writepois
    
    from math import ceil,log10
    for c in parnames:
        scale = max(abs(c_results[c][1]),abs(c_results[c][0]))
        c_pois[c]["scale"]=pow(10,-ceil(log10(scale)))
    writepois("Internal",c_pois,[({"color":"black"},c_results)],"eftSummaryWilson.tex",labels)

ev_pois = { ev:{} for ev in ev_names }
#ev_pois["ggH_top_ev1"]["scale"]=100
#ev_pois["cHq3"]["scale"]=10
#ev_pois["cHW_cHB_cHWB_cHDD_cuWAbs_cuBAbs_ev1"]["scale"]=10
#ev_pois["ggH_top_ev2"]["scale"]=1
#ev_pois["cHu_cHd_cHq1_ev1"]["scale"]=1
#ev_pois["cHl3_cll1_ev1"]["scale"]=0.1
#ev_pois["ggH_top_ev3"]["scale"]=0.1
#ev_pois["cHW_cHB_cHWB_cHDD_cuWAbs_cuBAbs_ev3"]["scale"]=0.1
#ev_pois["cHl1_cHe_ev1"]["scale"]=0.01

ev_pois['cW_01']["scale"]=0.1
ev_pois['cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01']["scale"]=100.0
ev_pois['cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02']["scale"]=0.1
ev_pois['cll1_cHl3_01']["scale"]=1.0
ev_pois['cll1_cHl3_02']["scale"]=0.1
ev_pois['clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01']["scale"]=1.0
ev_pois['clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02']["scale"]=1.0

ev_pois_list=['cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_01', 'cdHAbs_cHB_cuHAbs_cHbox_cHWB_cHW_cHDD_cuGAbs_cHG_02','clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_01', 'clu_clq3_clq1_cld_cHq3_cHq1_cHd_cHu_02','cll1_cHl3_01', 'cll1_cHl3_02','cW_01']
ev_pois_list.reverse()

dirs = ["linear"]

base = "red"
#directories = ["lin","lin_quad","lin_stat_only"]
exp = makeSummary(base,"exp",dirs,ev_pois,ev_pois_list)
obs = makeSummary(base,"obs",dirs,ev_pois,ev_pois_list)

makeTable("summary-table.tex",obs,exp,dirs)
#makeCSV("summary-table.csv",obs,exp,dirs)
#
#fitresult_ev_lin = "20200928_WS-Comb_rotated1_obs_set1_qf.root"
#from RooFitUtils.io import getCorrelation
#corr = getCorrelation(fitresult_ev_lin,parameters=ev_names)
#writematrix("Internal",ev_names,ev_names,corr,"correlations-eft.tex",-1,1,45,plotlabels=matlabels,showall=True,flip=True)
#writehepdatamatrix(ev_names,ev_names,corr,"correlations-eft.yml",label="correlations")
#writecsvmatrix(ev_names,ev_names,corr,"correlation-eft.data")

#makeErrorTable("summary-table-errors-obs.tex",obs["lin"],obs["lin_stat_only"])
#makeErrorTable("summary-table-errors-exp.tex",exp["lin"],exp["lin_stat_only"])

#try:
#    makeBackRotation(obs["lin"]["cv"])
#except RuntimeError:
#    pass
#    
