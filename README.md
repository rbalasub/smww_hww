# smww hww  

This package contains the configs used to build the combined workspace
for the smww+hww combination using workspaceCombiner. It also contains
plotting scripts, using the correlationplot from quickFit and other
plotting scripts from RooFitUtils. A set of scripts written by Carsten
Burgard for the hcomb conf 2020 is available in ~scripts/~ and is also
used for the plotting.

## Setup

This package supports setup within RootCore and CMake based ASG
releases as well as standalone compilation with ROOT.

In order to compile with CMake, type

    lsetup "cmake 3.9.6"
    lsetup "root 6.20.02-x86_64-centos7-gcc8-opt"
    source /cvmfs/sft.cern.ch/lcg/releases/LCG_96b/CMake/3.14.3/x86_64-centos7-gcc8-opt/CMake-env.sh;

    mkdir build; cd build;

To build RooFitUtils do,

    cmake ../RooFitUtils; make -j4;

Now, you are ready to use the package. Don't forget to 

    source setup.sh

every time you create a new shell.

