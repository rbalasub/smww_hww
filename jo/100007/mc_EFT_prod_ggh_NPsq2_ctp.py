def generate1(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,run_name='Test',proc_dir='PROC_mssm_0',grid_pack=False,gridpack_compile=False,cluster_type=None,cluster_queue=None,cluster_nb_retry=None,cluster_temp_path=None,extlhapath=None,madspin_card_loc=None,required_accuracy=0.01,gridpack_dir=None,nevents=None,random_seed=None,reweight_card_loc=None,bias_module=None):    # Just in case
    setup_path_protection()
    version = getMadGraphVersion() # avoiding code duplication
    # If we need to get the cards...
    if run_card_loc is not None and not os.access(run_card_loc,os.R_OK):
        if run_card_loc.count('/')>1:
            mglog.warning('Several / found in run card name '+str(run_card_loc)+' but card not found.')
            mglog.warning('Will try to retrieve the card via get files, without the path portion of the name.')
            run_card_loc = run_card_loc.split('/')[-1]
        mglog.info('Getting run card '+str(run_card_loc)+' via get_files')
        get_run_card = subprocess.Popen(['get_files','-data',run_card_loc])
        get_run_card.wait()
        if not os.access(run_card_loc,os.R_OK):
            mglog.error('Could not find run card '+str(run_card_loc))
            return 1
    if param_card_loc is not None and not os.access(param_card_loc,os.R_OK):
        if param_card_loc.count('/')>1:
            mglog.warning('Several / found in param card name '+str(param_card_loc)+' but card not found.')
            mglog.warning('Will try to retrieve the card via get files, without the path portion of the name.')
            param_card_loc = param_card_loc.split('/')[-1]
        mglog.info('Getting param card '+str(param_card_loc)+' via get_files')
        get_param_card = subprocess.Popen(['get_files','-data',param_card_loc])
        get_param_card.wait()
        if not os.access(param_card_loc,os.R_OK):
            mglog.error('Could not find param card '+str(param_card_loc))
            return 1
    if reweight_card_loc is not None and not os.access(reweight_card_loc,os.R_OK):
        mglog.error('Could not find reweight card '+str(reweight_card_loc))
        return 1
    # Check if process is NLO or LO
    isNLO=is_NLO_run(proc_dir=proc_dir)
    #Running in on-the-fly mode
    mglog.info('Started generating at '+str(time.asctime()))
    if param_card_loc is None: mglog.info('Will use the default parameter card')
    # Ensure that things are set up normally
    if not os.access(run_card_loc,os.R_OK):
        mglog.error('No run card found at '+run_card_loc)
        return 1
    if param_card_loc is not None and not os.access(param_card_loc,os.R_OK):
        mglog.error('No param card found at '+param_card_loc)
        return 1
    if reweight_card_loc is not None and not os.access(reweight_card_loc,os.R_OK):
        mglog.error('No reweight card found at '+reweight_card_loc)
        return 1
    if not os.access(proc_dir,os.R_OK):
        mglog.error('No process directory found at '+proc_dir)
        return 1
    if not os.access(proc_dir+'/bin/generate_events',os.R_OK):
        mglog.error('No generate_events module found in '+proc_dir)
        return 1
    try:
        os.remove(proc_dir+'/Cards/run_card.dat')
        if param_card_loc is not None: os.remove(proc_dir+'/Cards/param_card.dat')
    except:
        pass
    try:
        shutil.copyfile(run_card_loc,proc_dir+'/Cards/run_card.dat')
        if param_card_loc is not None: shutil.copyfile(param_card_loc,proc_dir+'/Cards/param_card.dat')
    except Exception as e:
        mglog.error('Trouble moving the cards into place!!')
        mglog.error('Issued exception '+str(e.args)+' '+str(e))
        mglog.error('Working from '+str(os.getcwd()))
        return 1
    allow_links=False    
    (LHAPATH,origLHAPATH,origLHAPDF_DATA_PATH) = setupLHAPDF(isNLO, version=version, proc_dir=proc_dir, extlhapath=extlhapath, allow_links=allow_links)
    mglog.info('For your information, the libraries available are (should include LHAPDF):')
    mglog.info( sorted( os.listdir( proc_dir+'/lib/' ) ) )
    setupFastjet(isNLO, proc_dir=proc_dir)
    if bias_module!=None:
        setup_bias_module(bias_module,run_card_loc,proc_dir,grid_pack)
    mglog.info('Now I will hack the make files a bit.  Apologies, but there seems to be no good way around this.')
    shutil.copyfile(proc_dir+'/Source/make_opts',proc_dir+'/Source/make_opts_old')
    old_opts = open(proc_dir+'/Source/make_opts_old','r')
    new_opts = open(proc_dir+'/Source/make_opts','w')
    for aline in old_opts:
        if 'FC=g' in aline:
            mglog.info('Configuring the fancy gfortran compiler instead of g77 / f77')
            new_opts.write('  FC=gfortran')
        elif 'FFLAGS+= -ffixed-line-length-132' in aline and 'i686' in os.environ['CMTCONFIG']:
            mglog.info('Setting you up for a 32-bit compilation')
            new_opts.write('FFLAGS+= -ffixed-line-length-132 -m32')
        else:
            new_opts.write(aline)
    old_opts.close()
    new_opts.close()
    mglog.info('Make file hacking complete.')
    print_cards(run_card=run_card_loc,param_card=(param_card_loc if param_card_loc is not None else proc_dir+'/Cards/param_card.dat'))
    currdir=os.getcwd()
    os.chdir(proc_dir)
    athenaMP = False
    if not isNLO:
        run_card_consistency_check(isNLO=isNLO)
        mglog.info('Running serial generation.  This will take a bit more time than parallel generation.')
        generate = subprocess.Popen(['bin/generate_events','-f',str(run_name),'--multicore --nb_core=16'],stdin=subprocess.PIPE)
        generate.communicate()
    elif isNLO:
        run_card_consistency_check(isNLO=isNLO)
        mygenerate = subprocess.Popen(['bin/generate_events','-f','--name='+str(run_name),'--multicore --nb_core=16'],stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
        mygenerate.communicate()
    os.chdir(currdir)
    resetLHAPDF(origLHAPATH=origLHAPATH,origLHAPDF_DATA_PATH=origLHAPDF_DATA_PATH)
    mglog.info('Finished at '+str(time.asctime()))
    return 0
from MadGraphControl.MadGraphUtils import *

#---------------------------------------------------------------------------------------------------
# parsing the Gen_tf.py arguments
#---------------------------------------------------------------------------------------------------

def parseargs(runArgs,safefactor=2.,nevents=1000):
    options = {}

    if hasattr(runArgs,'ecmEnergy'):
        options["beamEnergy"] = runArgs.ecmEnergy / 2.
    else: 
        raise RuntimeError("No center of mass energy found.")

    if hasattr(runArgs,"maxEvents") and runArgs.maxEvents > 0:
        options["nevents"]=runArgs.maxEvents*safefactor
    else:
        options["nevents"]=nevents*safefactor

    if hasattr(runArgs,"randomSeed"):
        options["randomSeed"] = runArgs.randomSeed
    else:
        raise RuntimeError("No random seed provided.")

    return options

options = parseargs(safefactor=2.0,runArgs=runArgs)


#---------------------------------------------------------------------------------------------------
# Passing the Process
#---------------------------------------------------------------------------------------------------

process_dir = new_process("""import model ./SMEFT/model/SMEFTatNLO_U2_2_U3_3_cG_4F_LO_UFO-NLO_ctp
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define jb = j b b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~

#------------------- ggh + bbh production only------------------------#
generate       g g > e+ mu- ve vm~                      [noborn=QCD] @0

output PROC_prod_gghbbh_NLO_ctp
""")

settings = {
            'dparameter':'0.4',
            'pdlabel':'lhapdf',
            'lhaid':'90400',
            'ickkw':0,
            'ktdurham':30,
            'maxjetflavor': 5,
            'dparameter'  : 0.4,
            'ptl':20,
            'etal':2.7,
            'drll':0.05,
            'ptb':5,
            'drjj':0.0,
            'drbb': 0.0,
            'drbj': 0.0,
}
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',nevts=int(options["nevents"]),rand_seed=options["randomSeed"],beamEnergy=options["beamEnergy"],xqcut=0.,extras=settings)


#---------------------------------------------------------------------------------------------------
# Calling Madgraph
#---------------------------------------------------------------------------------------------------

os.environ["ATHENA_PROC_NUMBER"] = "16"
runName = "run_01"
generate1(run_card_loc='run_card.dat',param_card_loc=process_dir+'/Cards/param_card_default.dat',proc_dir=process_dir,run_name=runName)
arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',lhe_version=3.0,saveProcDir=False)

evgenConfig.inputfilecheck = runName
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'
#---------------------------------------------------------------------------------------------------
# Pythia8 Showering with A14_NNPDF23LO
#---------------------------------------------------------------------------------------------------

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
ktdurham = 30
dparameter = 0.4
PYTHIA8_nJetMax=2
PYTHIA8_Dparameter=dparameter                                                              
PYTHIA8_TMS=ktdurham
PYTHIA8_Process="pp>h"
PYTHIA8_nQuarksMerge=5
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]



evgenConfig.keywords = ['insert', 'keywords', 'here']
evgenConfig.contact = ['Insert e-mail id']
evgenConfig.generators = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.description = """madgraph process."""

TestHepMC.m_maxFailures = 100
#---------------------------------------------------------------------------------------------------
# For debugging purposes, test this joboption with the following command line:
# Gen_tf.py --ecmEnergy=13000 --firstEvent=1 --randomSeed=1 --jobConfig=/path/to/jobOption/ --outputEVNTFile=EVNT.00001.root --maxEvents=10
#---------------------------------------------------------------------------------------------------

